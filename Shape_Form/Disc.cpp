/* 
 * File:   Disc.cpp
 * Author: edouard
 * 
 * Created on 4 novembre 2013, 13:52
 */

#include "Disc.hpp"
#include <iostream>
using namespace std;
    Disc::Disc():center(), radius(){
        
    }
    
    Disc::Disc(float radius,const Point3D& center, const Color color):radius(radius){
        this->center = center;
        this->center.setColor(color);
        cout << "Disc" << endl;
    }
    
    Disc::Disc(const Disc&){
        
    }
    
    Disc::~Disc(){
        this->center.~Point3D();
        cout <<  "~Disc" << endl;
    }

    const Point3D& Disc::getCenter() const {
        return center;
 }

    void Disc::setCenter(const Point3D& center) {
        this->center = center;
    }

    const float& Disc::getRadius() const {
        return radius;
    }

    void Disc::setRadius(const float& radius) {
        this->radius = radius;
    }

    float Disc::getArea() const{
        return M_PI*(this->radius)*(this->radius);
    }
    
    void Disc::draw() const{
        this->center.print();
        cout << this->radius << endl;
    }
