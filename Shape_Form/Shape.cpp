/* 
 * File:   Shape.cpp
 * Author: francois
 * 
 * Created on November 4, 2013, 1:49 PM
 */

#include "Shape.hpp"
#include <iostream>
using namespace std;
int Shape::idShape = 0;
Shape::Shape() {
    this->id = getNewId();
    this->setColor(0,0,0);
    cout << "Shape() : " << this->id <<endl;
}

Shape::Shape(const Color& orig) {
    this->id = getNewId();
    this->setColor(orig);
    cout << "Shape(const Color&) : " << this->id << endl;
}

Shape::Shape(const Shape& orig) {
    
    this->id = getNewId();
    this->setColor(orig.getColor());
    cout << "Shape(const Shape&) : " << this->id << endl;
}

Shape::~Shape() {
    cout << "~Shape() : " << this->id << endl;
}

void Shape::setColor(int r, int g, int b) {
    this->c.SetColor(r,g,b);
}

void Shape::setColor(const Color& orig) {
    this->c.SetColor(orig.GetR(),orig.GetG(), orig.GetB());
}
const Color & Shape::getColor() const {
    return this->c;
}
int Shape::getNewId() {
    idShape++;
    return idShape;
}
float Shape::getArea() const {
    return 0;
}
void Shape::draw() const {
    cout << "draw()" << endl;
    cout << "idShape : "<< this->id << endl;
    cout << "Color : " << "(" << this->c.GetR()<< "," <<this->c.GetG()<< "," <<this->c.GetB()<<")" << endl;
}