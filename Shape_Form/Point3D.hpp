/* 
 * File:   Pointed3D.hpp
 * Author: elerion
 *
 * Created on 4 novembre 2013, 14:16
 */

#ifndef POINTED3D_HPP
#define	POINTED3D_HPP
#include "Color.hpp"
#include "main.hpp"
class Point3D {
private:
            float x,y,z;
            Color color;
            static int nbInst;
	public:
            Point3D(const float=0,const float=0,const float=0,const int=0, const int=0, const int=0);
            Point3D(const Point3D &);
            Point3D(const Color &);
            ~Point3D();
            void initialize(const float=0,const float=0,const float=0,const int=0, const int=0, const int=0);
            void reset(); //reset coord to 0,0,0
            float moveTo(Point3D&); //move to another point, returns move distance
            float moveTo(float=0,float=0,float=0); //move to coords, returns distance
            void decr(); //decrease all coords with -1
            void incr(); //increase all coords with +1
            void print()const; // display a Point in the following way: (x,y,z) - EnumColor
            void colorize(Color);
            const float getX() const;
            const float getY() const;
            const float getZ() const;
            void setX(const float);
            void setY(const float);
            void setZ(const float);
            void setColor(const int, const int, const int);
            void setColor(const Color color);
            void createRoad(int);
            Point3D & getPoint3DOnRoad(int, int);
            float distanceRoad(int);
            const Color getColor()const;
            static int getNbInst();

};

#endif	/* POINTED3D_HPP */

