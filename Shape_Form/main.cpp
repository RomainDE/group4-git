/* 
 * File:   main.cpp
 * Author: elerion
 *
 * Created on 4 novembre 2013, 13:38
 */



/*
 * 
 */
#include "Disc.hpp"

using namespace std;

int main(int argc, char** argv) {
    Point3D p0;
    Color color0;
    Disc disc0 = Disc(1, p0, color0);

    cout << "------------------------------" << endl;
    disc0.draw();
    cout << "------------------------------" << endl;
    Shape* pShape0 = &disc0;
    pShape0->draw();
    cout << "------------------------------" << endl;
    disc0.setCenter(Point3D(1, 1, 1));
    cout << "centre=(" << disc0.getCenter().getX() << ";" << disc0.getCenter().getY() << ";" << disc0.getCenter().getZ() << ")" << endl;
    disc0.draw();
    disc0.setRadius(2);
    cout << "radius=" << disc0.getRadius() << endl;
    disc0.draw();
    disc0.setColor(Color(255, 255, 255));
    cout << "color= (" << disc0.getColor().GetR() << ";" << disc0.getColor().GetG() << ";" << disc0.getColor().GetB() << ")" << endl;
    disc0.draw();
    cout << "area=" << disc0.getArea() << endl;
    //
    cout << "------------------------------" << endl;
    Shape shape0 = disc0;
    shape0.draw();
    //erreur les fonction fille ne peuvent pas etre utilisées par la classe mere
//    cout << "------------------------------" << endl;
//    shape0.setCenter(Point3D(1, 1, 1));
//    cout << "centre=(" << shape0.getCenter().getX() << ";" << shape0.getCenter().getY() << ";" << shape0.getCenter().getZ() << ")" << endl;
//    shape0.draw();
//    shape0.setRadius(2);
//    cout << "radius=" << shape0.getRadius();
//    shape0.draw();
//    shape0.setColor(Color(255, 255, 255));
//    cout << "color= (" << shape0.getColor().r << ";" << shape0.getColor().g << ";" << shape0.getColor().b << ")" << endl;
//    shape0.draw();
//    cout << "area=" << shape0.getArea() << endl;
//    cout << "------------------------------" << endl;
//    //
//    cout << "------------------------------" << endl;
//    Shape* pShape1 = &disc0;
//    pShape1->draw();
//    cout << "------------------------------" << endl;
//    pShape1->setCenter(Point3D(1, 1, 1));
//    cout << "centre=(" << pShape1->getCenter().getX() << ";" << pShape1->getCenter().getY() << ";" << pShape1->getCenter().getZ() << ")" << endl;
//    pShape1->draw();
//    pShape1->setRadius(2);
//    cout << "radius=" << pShape1->getRadius();
//    pShape1->draw();
//    pShape1->setColor(Color(255, 255, 255));
//    cout << "color= (" << pShape1->getColor().r << ";" << pShape1->getColor().g << ";" << pShape1->getColor().b << ")" << endl;
//    pShape1->draw();
//    cout << "area=" << pShape1->getArea() << endl;
//    cout << "------------------------------" << endl;
    //
//    cout << "------------------------------" << endl;
//    Disc disc1 = *pShape1;
//    disc1.draw();
//    cout << "------------------------------" << endl;
//    Shape* pShape2 = &disc1;
//    pShape2->draw();
//    cout << "------------------------------" << endl;
//    disc1.setCenter(Point3D(1, 1, 1));
//    cout << "centre=(" << disc1.getCenter().getX() << ";" << disc1.getCenter().getY() << ";" << disc1.getCenter().getZ() << ")" << endl;
//    disc1.draw();
//    disc1.setRadius(2);
//    cout << "radius=" << disc1.getRadius() << endl;
//    disc1.draw();
//    disc1.setColor(Color(255, 255, 255));
//    cout << "color= (" << disc1.getColor().GetR() << ";" << disc1.getColor().GetG() << ";" << disc1.getColor().GetB() << ")" << endl;
//    disc1.draw();
//    cout << "area=" << disc1.getArea() << endl;
//    //
//    cout << "------------------------------" << endl;
//    Disc* pDisc0 = pShape1;
//    pDisc0->draw();
//    cout << "------------------------------" << endl;
//    Shape* pShape3 = pDisc0;
//    pShape3->draw();
//    cout << "------------------------------" << endl;
//    pDisc0->setCenter(Point3D(1, 1, 1));
//    cout << "centre=(" << pDisc0->getCenter().getX() << ";" << pDisc0->getCenter().getY() << ";" << pDisc0->getCenter().getZ() << ")" << endl;
//    pDisc0->draw();
//    pDisc0->setRadius(2);
//    cout << "radius=" << disc1.getRadius() << endl;
//    pDisc0->draw();
//    pDisc0->setColor(Color(255, 255, 255));
//    cout << "color= (" << pDisc0->getColor().r << ";" << pDisc0->getColor().g << ";" << pDisc0->getColor().b << ")" << endl;
//    pDisc0->draw();
//    cout << "area=" << pDisc0->getArea() << endl;

    return 0;
}


