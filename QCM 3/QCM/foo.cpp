#include "foo.hpp"
#include <iostream>
using namespace std;
 
Foo::Foo():i(0)
{
        cout << "Foo: default constructor" << endl;
}
 
Foo::Foo(int i):i(i)
{
         cout << "Foo constructor" << endl;
}
 
Foo::Foo(const Foo& f):i(f.i){
        cout << "Foo: copy constructor" << endl;
}
 
void Foo::setI(int i)
{
        this->i = i;
}
 
void Foo::print() const{
        cout << "Foo: i=" << this->i << endl;
}
 
Foo::~Foo()
{
        cout << "Foo: destructor" << endl;
}
 