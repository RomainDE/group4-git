
/* REPONSES
 
 
1.              5
2.              4
3.              2
4.              2
5.              1
6.              1
7.              4
8.              1
9.              4
10.             4
11.             4
 
 
*/
 
 
#ifndef Foo_H
#define Foo_H
 
class Foo {
        private:
                int i;
        public:
                Foo();
                Foo(int);
                Foo(const Foo&);
                void setI(int);
                void print() const;
                ~Foo();
};
 
#endif
 
 
 