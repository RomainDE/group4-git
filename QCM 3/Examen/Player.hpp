/* 
 * File:   Player.hpp
 * Author: francois
 *
 * Created on October 14, 2013, 1:58 PM
 */
enum EnumPost {attaquant, milieu, defenseur, gardien};
#ifndef PLAYER_HPP
#define	PLAYER_HPP
#include"String.hpp"
class Player {
public:
    Player(String = "NoName", EnumPost=attaquant, bool = 0);
    Player(const Player& orig);
    virtual ~Player();

    bool IsDisponible() const {
        return disponible;
    }

    void SetDisponible(bool disponible) {
        this->disponible = disponible;
    }

    String GetName() const {
        return name;
    }

    void SetName(String name) {
        this->name = name;
    }
    EnumPost GetPost() const {
        return post;
    }
    void SetPost(EnumPost post) {
        this->post = post;
    }

private:
    String name;
    EnumPost post;
    bool disponible;
};
#endif	/* PLAYER_HPP */

