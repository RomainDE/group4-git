/* 
 * File:   String.hpp
 * Author: francois
 *
 * Created on October 14, 2013, 1:58 PM
 */

#ifndef STRING_HPP
#define	STRING_HPP
#include <string.h>
class String {
public:
    String();
    String(const char*);
    String(const String& orig);
    virtual ~String();
    void setName(const char*);
    const char* const getName() const;
private:
    char* name;
    
};

#endif	/* STRING_HPP */

