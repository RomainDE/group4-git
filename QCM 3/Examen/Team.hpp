/* 
 * File:   Team.hpp
 * Author: francois
 *
 * Created on October 14, 2013, 1:57 PM
 */

#ifndef TEAM_HPP
#define	TEAM_HPP
#include"Player.hpp"
#include"Coach.hpp"
#include <cstdlib>
#include <iostream>
using namespace std;
class Team {
public:
    Team(String = "NoClubName",String = "NoCoachName", String ="NoPlayerName", bool =0,EnumPost =attaquant );
    Team(const Team& orig);
    virtual ~Team();
    String GetClubName() const {
        return clubName;
    }

    void SetClubName(String clubName) {
        this->clubName = clubName;
    }

    Coach GetTrainer() const {
        return trainer;
    }
    int GetNbPlayers() {
        return sizeof(players);
    }
    void SetTrainer(Coach trainer) {
        this->trainer = trainer;
    }
    Player* GetPlayers() const {
        return players;}

    void SetPlayers(Player* players) {
        this->players = players;
    }
    void AddPlayer(Player player) {
        if (this->GetNbPlayers() < 30) {
         this->players[sizeof(this->players)-GetNbPlayers()+1] = player;
        }
    }

private:
    String clubName;
    Player* players;
    Coach trainer;
    

};

#endif	/* TEAM_HPP */

