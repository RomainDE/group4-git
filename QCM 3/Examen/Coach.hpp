/* 
 * File:   Coach.hpp
 * Author: francois
 *
 * Created on October 14, 2013, 1:58 PM
 */

#ifndef COACH_HPP
#define	COACH_HPP
#include"String.hpp"
class Coach {
public:
    Coach();
    Coach(const Coach& orig);
    Coach(String name);
    virtual ~Coach();
    String GetName() const {
        return name;
    }
    
    void SetName(String name) {
        this->name = name;
    }

private:
    String name;
};

#endif	/* COACH_HPP */

