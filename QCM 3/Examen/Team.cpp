/* 
 * File:   Team.cpp
 * Author: francois
 * 
 * Created on October 14, 2013, 1:57 PM
 */

#include "Team.hpp"
Team::Team( String clubName, String coachName, String playerName, bool disponibility,EnumPost post) : trainer(coachName){
    
    this->SetClubName(clubName);
    this->players = new Player[30];
    this->players[0].SetName(playerName);
    this->players[0].SetDisponible(disponibility);
    this->players[0].SetPost(post);
}

Team::Team(const Team& orig) {
    this->SetClubName(orig.GetClubName());
    this->SetPlayers(orig.GetPlayers());
    this->SetTrainer(orig.GetTrainer());
}

Team::~Team() {
    for (int i = 0; i < 30; i++) {
        delete(this->players);
    }

}



