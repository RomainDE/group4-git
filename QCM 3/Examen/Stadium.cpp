/* 
 * File:   Stadium.cpp
 * Author: francois
 * 
 * Created on October 14, 2013, 1:59 PM
 */

#include "Stadium.hpp"

Stadium::Stadium() {
    this->SetName("NoName");
    this->SetLocalisation("NoLocalisation");
}

Stadium::Stadium(const Stadium& orig) {
    this->SetName(orig.GetName());
    this->SetLocalisation(orig.GetLocalisation());
}

Stadium::~Stadium() {
}

