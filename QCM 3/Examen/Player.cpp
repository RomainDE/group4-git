/* 
 * File:   Player.cpp
 * Author: francois
 * 
 * Created on October 14, 2013, 1:58 PM
 */

#include "Player.hpp"

Player::Player(String name, EnumPost post, bool disponibility) {
    this->SetName(name);
    this->SetPost(post);
    this->SetDisponible(disponibility);
}

Player::Player(const Player& orig) {
    this->SetName(orig.GetName());
    this->SetDisponible(orig.IsDisponible());
    this->SetPost(orig.GetPost());
}

Player::~Player() {
}

