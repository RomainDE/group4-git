/* 
 * File:   Coach.cpp
 * Author: francois
 * 
 * Created on October 14, 2013, 1:58 PM
 */

#include "Coach.hpp"
#include "Player.hpp"

Coach::Coach() {
    this->SetName("NoName");
}
Coach::Coach(String name) {
    this->SetName(name);
}
Coach::Coach(const Coach& orig) {
    this->SetName(orig.GetName());
}
Coach::~Coach() {
    
}

