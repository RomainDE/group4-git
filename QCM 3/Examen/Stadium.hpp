/* 
 * File:   Stadium.hpp
 * Author: francois
 *
 * Created on October 14, 2013, 1:59 PM
 */

#ifndef STADIUM_HPP
#define	STADIUM_HPP
#include "String.hpp"
#include"Team.hpp"
class Stadium {
public:
    Stadium();
    Stadium(const Stadium& orig);
    virtual ~Stadium();
    String GetLocalisation() const {
        return localisation;
    }

    void SetLocalisation(String locaisation) {
        this->localisation = locaisation;
    }

    String GetName() const {
        return name;
    }

    void SetName(String name) {
        this->name = name;
    }

private:
    String name;
    String localisation;
};

#endif	/* STADIUM_HPP */

