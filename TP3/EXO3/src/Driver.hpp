#include <iostream>
#include <string.h>
using namespace std;

struct driver{
	char *name;
	int number;
};

void init(driver *);
void init(driver *, char *, int =0) ;
char * get_name(const driver &) ;
int get_number(const driver &) ;
void set_name(driver*,char*) ;
void set_number(driver* , int) ;
int test_equality(const driver&,const driver &) ;
int test_equality(const driver &,char*) ;
int test_equality(const driver &,int) ;
void copy(driver* driverPtr, driver aDriver) ;
void print(const driver &) ;