#include <iostream>
using namespace std;

void implicite_0(float,int=0,int=0);
//les paramètres avec des valeurs par défaut doivent être à la fin

void implicite_1_1(int ,float=2.f,char='a');
void implicite_1_2(int);

void implicite_2_1(int=7);
void implicite_2_2(void);

void implicite_3(float=7.0,int=7);
//les paramètres avec des valeurs paxr défaut doivent être à la fin
struct Point{
	float abscisse;
	float ordonnee;
};
Point p={4.3f, 9.9F};
void implicite_4(Point x=p);


void implicite_0(float z,int i, int j){
	cout<<"void implicite_0(int,int,float)"<<endl;
}

void implicite_1_1(int n,float x, char c){
	cout<<"void implicite_1_1(int ,float=2.f,char='a')"<<endl;
}

void implicite_1_2(int){
	cout<<"void implicite_1_2(int)"<<endl;
}

void implicite_2_1(int){
	cout<<"void implicite_2_1(int=7)"<<endl;
}

void implicite_2_2(void){
	cout<<"void implicite_2_2(void)"<<endl;
}

void implicite_3(float, int){
	cout<<"void implicite_2(int=7)"<<endl;
}

void implicite_4(Point p){
	cout<<"void implicite_4(Point={4.3f,9.9f})"<<endl;
}

int main(){
	implicite_0(1);

	float u=6.8f;

	implicite_1_1(12,u);
	implicite_1_2(12); //On différencie les deux fonctions implicite_1 par leur nom

	implicite_2_1(19);
	implicite_2_2();

	implicite_3();
	implicite_4();

	return 0;
}
