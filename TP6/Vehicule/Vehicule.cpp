/* 
 * File:   Vehicule.cpp
 * Author: francois
 * 
 * Created on October 7, 2013, 1:52 PM
 */

#include <algorithm>

#include "Vehicule.hpp"
Vehicule::Vehicule(char* name, int number, const float x, const float y, const float z, const char* color, const unsigned int speed, const char* label) : pilote(name, number), position(x, y, z, color){
    SetSpeed(speed);
    SetLabel(label);
    //cout << "Vehicle : "<< this <<"Pilot :"<< &this->pilote <<" Position :" << &this->position <<" Speed :"<< &this->speed <<" Label :"<< &this->label << endl;
}
Vehicule::~Vehicule() {
    
    //cout << this <<endl;
}

void Vehicule::stop(){
    this->SetSpeed(0);
}

void Vehicule::reset(){
    this->stop();
    this->position.moveTo();
}
float Vehicule::moveTo(const Point3D& point) {
    return this->position.moveTo(point);
}
float Vehicule::moveTo (const Vehicule& vehicle) {
    return this->position.moveTo(vehicle.position);
}
float Vehicule::moveTo (float dx,float dy,float dz) {
    return this->position.moveTo(dx, dy, dz);
}
void Vehicule::print() const{
    cout <<"Position :"<<"("<<this->position.getX()<<","<<this->position.getY()<<","<<this->position.getZ()<<")"<<"-"<<"Color :" <<this->position.getColor().getName()<<"-"<<"Speed :"<<this->GetSpeed()<<endl;
}
