/* 
 * File:   Point3D.hpp
 * Author: francois / Romain
 *
 * Created on September 23, 2013, 2:16 PM
 */

#ifndef POINT3D_HPP
#define	POINT3D_HPP
#include"String.hpp"
#include "main.hpp"
#ifdef	__cplusplus
extern "C" {
#endif

class Point3D{
	private:
            float x,y,z;
            String color;
            static int nbInst;
	public:
            Point3D();
            Point3D(const float,const float,const float,const char*);
            ~Point3D();
            void initialize(const float=0,const float=0,const float=0,const char* ="white");
            void reset(); //reset coord to 0,0,0
            float moveTo(const Point3D&); //move to another point, returns move distance
            float moveTo(float=0,float=0,float=0); //move to coords, returns distance
            void decr(); //decrease all coords with -1
            void incr(); //increase all coords with +1
            void print(); // display a Point in the following way: (x,y,z) - EnumColor
            void colorize(String);
            const float getX() const;
            const float getY() const;
            const float getZ() const;
            const String getColor() const;
            void setX(const float);
            void setY(const float);
            void setZ(const float);
            void setColor(const char*);
            void createRoad(int);
            Point3D & getPoint3DOnRoad(int, int);
            float distanceRoad(int);
            static int getNbInst();
};





#ifdef	__cplusplus
}
#endif

#endif	/* POINT3D_HPP */

