/* 
 * File:   Paddock.cpp
 * Author: elerion
 * 
 * Created on 12 octobre 2013, 12:18
 */

#include "Paddock.hpp"

const int Paddock::capacity = 5;

Paddock::Paddock(const char* label):label(label), number(0){
    Vehicule *tmp = new Vehicule[capacity];
    vehicule = &tmp;
    delete[] tmp;
}

Paddock::Paddock(const Paddock& orig) {
    this->vehicule = orig.vehicule;
    this->label = String(orig.label);
    this->number = orig.number;
}

Paddock::~Paddock() {
    //delete[] this->vehicule[0];
    //delete this->vehicule;
}

void Paddock::AddVehicule(Vehicule vehi){
    if(this->GetNumber() < GetCapcity()){
        this->SetNumber(this->GetNumber() + 1);
        this->vehicule[0][this->GetNumber()] = Vehicule(vehi);
    }else{
        cout << "Erreur capacity" << endl;
    }
}
const int& Paddock::GetCapcity(){
    return capacity;
}


const int Paddock::SearchByName(const char* search){
    String searchString  = String(search);
    bool find = -1;
    for(int i = 0; i < this->GetNumber(); i++){
       if(searchString.cmpChar(this->vehicule[0][i].GetLabel())){
           find = i;
       } 
    }
    if(find == -1){
        cout << "Not found" <<endl;
    }else{
        cout << "Found" << endl;
    }
    return find;
}


void Paddock::DeleteByName(const char* name){
    int index  = SearchByName(name);
    if(index != -1){
        this->DeleteByIndex(index);
    }
}

void Paddock::DeleteByIndex(const int index){
    if(index != (this->GetNumber() - 1)){
        this->vehicule[0][index].~Vehicule();
    }
    this->SetNumber(this->GetNumber() - 1);
}

Vehicule* Paddock::GetVehicule(){
    return this->vehicule[0];
}