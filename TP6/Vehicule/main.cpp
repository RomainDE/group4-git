/* 
 * File:   main.cpp
 * Author: francois
 *
 * Created on October 7, 2013, 1:52 PM
 */

#include <cstdlib>

#include "Vehicule.hpp"
#include "Paddock.hpp"

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    Paddock test;
    Vehicule* vehicule = test.GetVehicule();
    for(int i = 0; i < test.GetNumber(); i++){
        vehicule[i].print();
    }
    cout << test.GetNumber() << endl;
    return 0;
}

