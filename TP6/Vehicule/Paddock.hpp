/* 
 * File:   Paddock.hpp
 * Author: elerion
 *
 * Created on 12 octobre 2013, 12:18
 */

#ifndef PADDOCK_HPP
#define	PADDOCK_HPP

#include "Vehicule.hpp"
#include "main.hpp"
#include "String.hpp"


class Paddock {
public:
    Paddock(const char* ="Undefined");
    Paddock(const Paddock& orig);
    ~Paddock();
    int GetNumber() const {return number;};
    void SetNumber(const int number) {this->number = number;};
    static const int& GetCapcity();
    void AddVehicule(Vehicule);
    const int SearchByName(const char*); // search by pilote name
    void DeleteByName(const char *); //delete by pilote name (use SearchByName)
    void DeleteByIndex(const int); //detele by index
    Vehicule* GetVehicule();

private:
    Vehicule** vehicule;
    const static int capacity;
    int number = 0;
    String label;

};

#endif	/* PADDOCK_HPP */

