/* 
 * File:   Vehicule.hpp
 * Author: francois
 *
 * Created on October 7, 2013, 1:52 PM
 */

#ifndef VEHICULE_HPP
#define	VEHICULE_HPP
#include "Point3D.hpp"
#include "Driver.hpp"
#include "main.hpp"

class Vehicule {
public:
    Vehicule(char* = "",int =0, const float=0, const float=0, const float=0, const char* ="white", const unsigned int = 0, const char* = "");
    virtual ~Vehicule();
    const char* GetLabel() const {return label.getName();}
    Driver GetPilote() const { return pilote; }
    Point3D GetPosition() const {return position;}
    unsigned int GetSpeed() const {return speed;}
    void SetLabel(const char* label) {this->label.setName(label);}
    void SetPilote(char* name){this->pilote.setName(name);}
    void SetPosition(Point3D position){this->position.moveTo(position);}
    void SetSpeed(int speed) {this->speed = speed;}
    void stop(); //reset vitesse
    float moveTo(const Point3D &); //Déplace la position vers un Point3D
    float moveTo(const Vehicule &); // Déplace la postion vers un autre vehicule
    float moveTo(float dx, float dy, float dz); //fait un déplacement
    void print() const; //-position (x,y,z) - couleur " " - vitesse : 
    void reset();
    
private:
    Point3D position;
    Driver pilote;
    unsigned int speed;
    String label;
};

#endif	/* VEHICULE_HPP */

