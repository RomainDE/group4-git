/* 
 * File:   String.cpp
 * Author: elerion
 * 
 * Created on 30 septembre 2013, 14:27
 */

#include "String.hpp"

String::String() : name(NULL){
    setName("");
}

String::String(const char* name) : name(NULL){
    setName(name);
}

String::String(const String& orig) {
    this->setName(orig.getName());
}

String::~String() {
    delete[] this->name;
}

void String::setName(const char* name){
    delete[] this->name;
    this->name = new char[sizeof(name)];
    strcpy(this->name, name);
}
const char* const String::getName() const{
    return this->name;
}
bool String::cmpChar(const char* cmp){
    if(strcmp(this->getName(), cmp) == 0){
        return true;
    }else{
        return false;
    }
}
