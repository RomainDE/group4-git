/* 
 * File:   String.hpp
 * Author: elerion
 *
 * Created on 30 septembre 2013, 14:27
 */

#ifndef STRING_HPP
#define	STRING_HPP
#include "main.hpp"
class String {
public:
    String();
    String(const char*);
    String(const String& orig);
    virtual ~String();
    void setName(const char*);
    const char* const getName() const;
    bool cmpChar(const char* cmp);
private:
    char* name;

};

#endif	/* STRING_HPP */

