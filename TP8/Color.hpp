 /* 
 * File:   Color.hpp
 * Author: elerion
 *
 * Created on 4 novembre 2013, 13:52
 */

#ifndef COLOR_HPP
#define	COLOR_HPP
#include "main.hpp"
#include "BadColor.h"
class Color {
public:
    Color(int R = 0, int G = 0, int B = 0):r(R), g(G), b(B){}  
    Color(const Color& orig):r(orig.GetR()), g(orig.GetG()), b(orig.GetB()){}
    virtual ~Color(){};
    const int GetB() const {return b;}
    const int GetG() const {return g;}
    const int GetR() const {return r;}
    void SetColor(const int r, const int g, const int b){ this->SetR(r); this->SetG(g); this->SetB(b);}
    void SetColor(const Color color){this->SetColor(color.GetR(), color.GetG(), color.GetB());}
    void print()const{std::cout << "R = " << this->GetR() << " G = " << this->GetG() << " B  = " << this->GetB() << endl;}
    
private:
    int r;
    int g;
    int b;
    void SetB(const int b_) {
        if ((b_ >= 0) && (b_<= 255)){
            this->b = b_;
        }else{
            throw BadColor(b_, "blue");
        }
    }
    void SetG(const int g_) {
        if ((g_ >= 0) && (g_<= 255)){
            this->g = g_;
        }else{
            throw BadColor(g_, "green");
        }
    }
    void SetR(const int r_) {
        if ((r_ >= 0) && (r_<= 255)){
            this->r = r_;
        }else{
             throw BadColor(r_, "red");
        }
    }
    Color operator=(const Color& orig){
        this->SetColor(orig);
        return *this;
    }
    
};

#endif	/* COLOR_HPP */

