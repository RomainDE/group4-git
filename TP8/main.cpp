#include "MyWindow.hpp"
#include "Disc.hpp"
#include "Ellipse.hpp"
#include "BadRadius.h"
#include "Square.hpp"
#include "ShapeException.h"
#include "BadWindow.hpp"
#include "Rectangle.hpp"
#include "BadEllipse.h"
#include "Vehicule.hpp"
#include "VehiculusHybridus.h"
#include "VahiculeElectrique.hpp"
#include "VehiculeEssence.hpp"
#include "Paddock.hpp"
#include "Moto.h"
#include "Truck.h"
#include <unistd.h>

void castTests(MyWindow* win);
template <class T> void setEnergy(float, T);
template <class T> void setVehiculeEnergy(float, T*);
int main(int argc, char** argv) {
    MyWindow* win = NULL;
    try {
//                Disc disc0 = Disc(400, 300, 0);
//               disc0.setColor(255, 20, 0);
        //
        //        disc0.setRadius(10);
        //      win = MyWindow::createWindow(0, 0, 800, 600);
        //
        //        win->draw(disc0);
                    Ellipse ellipse0 = Ellipse(500, 600, 0);
            ellipse0.setRadius1(80);
            ellipse0.setRadius2(50);
            ellipse0.setColor(255,130,0);
        //    win->draw(ellipse0);
        //  
        //
        //    disc0.setColor(255, 0, 0);
        //    disc0.setRadius(100);
        //    Square square0 = Square(200, 300, 0);
        //    square0.setColor(20, 50, 60);
        //    square0.setC(200);
        //    win->draw(square0);
        //    win->draw(disc0);
        //    castTests(win);
        //    
        //    Rectangle rect0 = Rectangle(150, 150, 0, 150, 200);
        //    rect0.setColor(255, 130, 0);
        //    win->draw(rect0);
           MyWindow* window = MyWindow::createWindow(0, 0, 800, 600);
//        //Main s1
//        // creation d'une shape
        Shape* s = new Disc(20, Point3D(100,100,0), Color(255,0,50));
//        // on dessine une premiere fois
//        window->draw(*(s));
//        sleep(1);
//        // on efface la fenetre
//        window->clearGraph();
//        // on deplace la shape
//        s->move(200, 200);
//        // on redessine la shape deplacee
//        window->draw(*(s));
//        sleep(1);

//        //Main s2
//        Vehicule* v2 = new VehiculusHybridus("Vehicule Hybrid", Color(255, 0, 0), Point3D(150, 150, 0),50);
//        v2->setRepresentation(s);
//        for (int turn = 0; turn < 10; turn++) {
//            cout << "turn=" << turn << endl;
//            // deplacement du vehicule	
//            v2->moveTurn();
//            // affichage du vehicule
//           window->draw(*v2);
//            // affichage des caracteristiques du vehicule (dont l'energie)
//           cout << *v2 << endl;
//            // temporisation
//            sleep(1);
//            // on efface l'affichage pour redessiner
//            window->clearGraph();
//        }
        Paddock<Moto> newCircuitMoto = Paddock<Moto>(10);
        setEnergy<Moto>(120, newCircuitMoto.GetCircuit()[1]);
        Paddock<Truck> newCircuitTruck = Paddock<Truck>(2);
        Paddock<VehiculeEssence> newCircuitVehicule = Paddock<VehiculeEssence>(5);
        setVehiculeEnergy(150, &newCircuitVehicule.GetCircuit()[0]);
        


        MyWindow::destroyWindow();
    } catch (ShapeException &br) {
        br.print();
        if (win != NULL) {
            MyWindow::destroyWindow();
        }

    } catch (BadWindow& bw) {
        bw.print();
        if (win != NULL) {
            MyWindow::destroyWindow();
        }
    } catch (BadColor & bc) {
        bc.print();
        if (win != NULL) {
            MyWindow::destroyWindow();
        }

    } catch (...) {
        cout << "default" << endl;
        if (win != NULL) {
            MyWindow::destroyWindow();
        }
    }




    //  
    //
    //    disc0.setColor(255, 0, 0);
    //    disc0.setRadius(100);
    //    Square square0 = Square(200, 300, 0);
    //    square0.setColor(20, 50, 60);
    //    square0.setC(200);
    //    win->draw(square0);
    //    win->draw(disc0);
    //    castTests(win);
    //    
    //    Rectangle rect0 = Rectangle(150, 150, 0, 150, 200);
    //    rect0.setColor(255, 130, 0);
    //    win->draw(rect0);

    return 0;
}


template <class T> void setEnergy(float energy_, T t) {
    t.setEnergy(energy_);
}

template <class T> void setVehiculeEnergy(float energy_, T* t) {
    if (dynamic_cast <Vehicule*> (t) != NULL) {
        dynamic_cast <Vehicule*> (t)->setEnergy(energy_);
    }
}