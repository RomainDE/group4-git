/* 
 * File:   DIsc.hpp
 * Author: edouard
 *
 * Created on 4 novembre 2013, 13:51
 */

#ifndef DISC_HPP
#define	DISC_HPP
#include"Point3D.hpp"
#include "Shape.hpp"

class Disc:public Shape {
private:
    Point3D center;
    float radius;

public:
    Disc();
    Disc(float x_=0, float y_=0, float z_=0);
    Disc(float radius,const Point3D& center, const Color);
    Disc(const Disc&);
    const float & getRadius()const;
    void setRadius(const float &);
    const Point3D & getCenter()const;
    void setCenter(const Point3D &);
    void move (float x_, float y_) {this->setCenter(Point3D(x_, y_));};
    float getArea() const;
    void draw(MyWindow*) const;
    
    Disc operator=(const Disc& orig){
        this->center = orig.getCenter();
        this->radius = orig.radius;
    }
    
    ~Disc();
   };

#endif	/* DISC_HPP */

