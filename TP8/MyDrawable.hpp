#ifndef MYDRAWABLE_H
#define	MYDRAWABLE_H
class MyWindow;
#include "MyWindow.hpp"
class Color;
#include "Color.hpp"

class MyDrawable {
public:
    MyDrawable();
    MyDrawable(const Color&);
    MyDrawable(const MyDrawable&);
    ~MyDrawable();
    void setColor(int r, int g, int b){
        this->c.SetColor(r, g, b);
    };
    void setColor(const Color& orig) {
        this->c.SetColor(orig);
    };
    const Color & getColor() const {
        return this->c;
    };
    virtual void draw(MyWindow*) const = 0;
    
//    MyDrawable operator=(const MyDrawable& orig){
//        this->c = orig.getColor();
//    }
    
private:
    Color c;
    

};


#endif	/* MYDRAWABLE_H */

