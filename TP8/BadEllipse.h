/* 
 * File:   BadEllipse.h
 * Author: elerion
 *
 * Created on 25 novembre 2013, 15:16
 */

#ifndef BADELLIPSE_H
#define	BADELLIPSE_H
#include "ShapeException.h"
#include "main.hpp"
class BadEllipse:public ShapeException {
public:
    BadEllipse(float orig1, float orig2):radius1(orig1),radius2(orig2){};
    BadEllipse(const BadEllipse& orig){this->radius1=orig.getRadius1(); this->radius2 = orig.getRadius2();};
    const float & getRadius1() const {return this->radius1;};
    const float & getRadius2() const {return this->radius2;};
    void print(){cout << "Rayon négatif = " << this->radius1 << " ou " << this->radius2 << " pour l'objet " << this << endl;};
    virtual ~BadEllipse(){};
private:
    float radius1;
    float radius2;
};

#endif	/* BADELLIPSE_H */

