/* 
 * File:   VehiculusHybridus.h
 * Author: francois
 *
 * Created on December 2, 2013, 3:06 PM
 */

#ifndef VEHICULUSHYBRIDUS_H
#define	VEHICULUSHYBRIDUS_H

#include "Turbo.h"
#include "VehiculeEssence.hpp"
#include "VahiculeElectrique.hpp"
#include "main.hpp"
#include "Point3D.hpp"
class VehiculusHybridus : public VehiculeEssence, public VahiculeElectrique  {
public:
    VehiculusHybridus (string intitule_, Color color_, Point3D point3d_,float speed_ = 0):VehiculeEssence(intitule_, color_,point3d_), VahiculeElectrique(intitule_, color_,point3d_), speed(speed_), Vehicule(intitule_, 0, color_, point3d_){
        this->setEnergy(100);
    };
    //VehiculusHybridus (const VehiculusHybridus & orig):speed(orig.speed), VehiculeEssence(, color_,point3d_), VahiculeElectrique(intitule_, color_,point3d_){};
    float getSpeed() {return this->speed;};
    void setEnergy(float ener_){
        this->energy = ener_;
    }
    float getEnergyUsage(float dist_) { return dist_*2,02;} ;
    void setSpeed(float speed) {
        this->speed = speed;
    }
    virtual ~VehiculusHybridus(){};
private:
    float speed;
};



#endif	/* VEHICULUSHYBRIDUS_H */

