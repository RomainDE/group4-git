#include "Vehicule.hpp"
#include "Shape.hpp"
Vehicule::Vehicule() : MyDrawable(), position(), energy(100), name("Vehicule"), representation(NULL) {
    cout << "Vehicule: protected constructor " << this << endl;
}

Vehicule::Vehicule(const string& n, float e, const Color& c, const Point3D& p) : MyDrawable(c), position(p), energy(e), name(n), representation(NULL) {
    cout << "Vehicule: constructor " << this << endl;
}

Vehicule::Vehicule(const Vehicule& v):MyDrawable(v.getColor()), position(v.position), energy(v.energy), name(v.name), representation(v.representation){
    cout << "Vehicule: copy constructor " << this << endl;
}

void Vehicule::draw(MyWindow* my) const {
    representation->move(position.getX(), position.getY());
    representation->draw(my);
}

void Vehicule::setRepresentation(Shape * rep) {
    delete representation;
    representation = rep;
    representation->setColor(getColor());
}

void Vehicule::moveTurn() {
    if (energy > 0) {
        float dp = getSpeed();
        float distance = position.moveTo(dp + position.getX(), position.getY(), position.getZ());
        energy -= getEnergyUsage(distance);
    }
};

const float & Vehicule::getEnergy() const {
    return energy;
};

const Point3D & Vehicule::getPosition() const {
    return position;
};

ostream & operator<<(ostream & os, const Vehicule& v_) {
        os << "Vehicule "<< v_.name << " : energy " << v_.getEnergy() << " - position (" << v_.getPosition().getX() << "," << v_.getPosition().getY() << ")" << "\n";
        return os;
};
    
void Vehicule::print() const {
    cout << "Vehicule "<< name << " : energy " << getEnergy() << " - position (" << getPosition().getX() << "," << getPosition().getY() << ")" << endl;
}

Vehicule::~Vehicule() {
    delete representation;
    cout << "Vehicule: destructor " << this << endl;
}

