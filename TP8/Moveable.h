/* 
 * File:   Moveable.h
 * Author: edouard
 *
 * Created on 2 décembre 2013, 13:52
 */

#ifndef MOVEABLE_H
#define	MOVEABLE_H
#include "main.hpp"
class Moveable {
public:
    Moveable(){cout << "Moveable constructor" << endl;};
    Moveable(const Moveable& orig){cout << "Moveable copy constructor" << endl;};
    virtual void move(float, float)=0;
    virtual ~Moveable(){cout << "Moveable destructor" << endl;};
private:

};

#endif	/* MOVEABLE_H */

