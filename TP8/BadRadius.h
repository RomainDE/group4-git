#include "ShapeException.h"
#include "main.hpp"
class BadRadius:public ShapeException {
private:
    float radius;
public:
    BadRadius(float orig):radius(orig) {};
    BadRadius(const BadRadius &orig) {this->radius = orig.getRadius();};
    const float & getRadius() const {return this->radius;};
    void print(){cout << "Rayon négatif = " << this->radius << " pour l'objet " << this << endl;};
    ~BadRadius(){};
};

