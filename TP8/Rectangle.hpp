/* 
 * File:   Rectangle.hpp
 * Author: elerion
 *
 * Created on 18 novembre 2013, 15:47
 */

#ifndef RECTANGLE_HPP
#define	RECTANGLE_HPP

#include "main.hpp"
#include "Shape.hpp"
#include "Point3D.hpp"

class Rectangle : public Shape {
public:
    Rectangle():center(), l(), L(){cout << "Rectangle : " << this << endl;}
    Rectangle(const float x_=0, const float y_=0, const float z_=0, const float l_=0, const float L_=0):center(x_, y_, z_), l(l_), L(L_){cout << "Rectangle : " << this << endl;}
    Rectangle(const Point3D orig, const Color color, const float l_, const float L_):center(orig), l(l_), L(L_){
        this->setColor(color);
        cout << "Rectangle : " << this << endl;
    }
    Rectangle(const Rectangle& orig):center(orig.GetCenter()), l(orig.Getl()), L(orig.GetL()){cout << "Copy Rectangle : " << this << endl;};
    virtual ~Rectangle(){}
    float GetL() const {return L;}
    void SetL(float L_) {this->L = L;}
    Point3D GetCenter() const {return center;}
    void SetCenter(Point3D center_) {this->center = center_;}
    float Getl() const {return l;}
    void Setl(float l_) {this->l = l;}
    float getArea() const{ return this->GetL() * this->Getl();}
    void move (float x_, float y_, float z_=0) {this->SetCenter(Point3D(x_, y_, z_));};
    void draw(MyWindow*) const;
    
    Rectangle operator=(const Rectangle& orig){
        this->SetCenter(orig.GetCenter());
        this->SetL(orig.GetL());
        this->Setl(orig.GetL());
    }
private:
    Point3D center;
    float l;
    float L;
};

#endif	/* RECTANGLE_HPP */

