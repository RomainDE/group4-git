#include "main.hpp"
#include "Color.hpp"
#include "Disc.hpp"
#include "MyDrawable.hpp"
#include "MyWindow.hpp"
#include "Point3D.hpp"
#include "Shape.hpp"
void castTests(MyWindow* win) {
    Point3D p0 = Point3D(350, 300, 0);
    Color color0;
    Disc disc0 = Disc(30, p0, color0);
    cout << "------------------------------" << endl;
    Shape* pShape0 = &disc0;
    win->draw(*pShape0);
    sleep(1);
    
    //    
    cout << "------------------------------ static_cast" << endl;
    Disc* pDisc0 = static_cast<Disc*> (pShape0);
    pDisc0->setCenter(Point3D(400, 250, 0));
    win->draw(*pDisc0);
    sleep(1);
    
    cout << "------------------------------" << endl;
    pDisc0->setCenter(Point3D(425, 325, 0));
    Shape* pShape1 = pDisc0;
    win->draw(*pShape1);
    sleep(1);
    
    cout << "------------------------------" << endl;
    pDisc0->setCenter(Point3D(250, 300, 0));
    cout << "centre=(" << pDisc0->getCenter().getX() << ";" << pDisc0->getCenter().getY() << ";" << pDisc0->getCenter().getZ() << ")" << endl;
    win->draw(*pDisc0);
    sleep(1);
    
    pDisc0->setRadius(50);
    cout << "radius=" << pDisc0->getRadius() << endl;
    win->draw(*pDisc0);
    sleep(1);
    
    pDisc0->setColor(Color(0, 0, 255));
    cout << "color= (" << pDisc0->getColor().GetR() << ";" << pDisc0->getColor().GetG() << ";" << pDisc0->getColor().GetB() << ")" << endl;
    win->draw(*pDisc0);
    sleep(1);
    
    cout << "area=" << pDisc0->getArea() << endl;
    //
    cout << "------------------------------ dynamic_cast" << endl;
    Disc* pDisc1 = dynamic_cast<Disc*> (pShape0);
    win->draw(*pDisc1);
    sleep(1);
    
    cout << "------------------------------" << endl;
    Shape* pShape2 = pDisc1;
    win->draw(*pShape2);
    sleep(1);
    
    cout << "------------------------------" << endl;
    pDisc1->setCenter(Point3D(400, 150, 0));
    cout << "centre=(" << pDisc1->getCenter().getX() << ";" << pDisc1->getCenter().getY() << ";" << pDisc1->getCenter().getZ() << ")" << endl;
    win->draw(*pDisc1);
    sleep(1);
    
    pDisc1->setRadius(25);
    cout << "radius=" << pDisc1->getRadius() << endl;
    win->draw(*pDisc1);
    sleep(1);
    
    pDisc1->setColor(Color(0, 255, 255));
    cout << "color= (" << pDisc1->getColor().GetR() << ";" << pDisc1->getColor().GetG() << ";" << pDisc1->getColor().GetB() << ")" << endl;
    win->draw(*pDisc1);
    sleep(1);
    cout << "area=" << pDisc1->getArea() << endl;
    
    pDisc1->setCenter(Point3D(250, 300, 0));
    win->draw(*pDisc1);
}
