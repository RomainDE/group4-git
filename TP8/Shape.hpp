#ifndef SHAPE_H
#define	SHAPE_H
class MyWindow;
#include "MyWindow.hpp"
#include "Moveable.h"
#include "MyDrawable.hpp"
class Shape : public MyDrawable, public Moveable {
private:
    int id;
    static int idShape;
protected:
    int getId();

public:
    Shape();
    Shape(const Color&);
    Shape(const Shape &);

    virtual float getArea() const = 0;
    virtual void draw(MyWindow*) const{};
    virtual void move(float, float){};
    ~Shape();
    
};

#endif	/* SHAPE_H */

