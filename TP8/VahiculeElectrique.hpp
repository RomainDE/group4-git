/* 
 * File:   VahiculeElectrique.hpp
 * Author: elerion
 *
 * Created on 2 décembre 2013, 14:42
 */

#ifndef VAHICULEELECTRIQUE_HPP
#define	VAHICULEELECTRIQUE_HPP

#include "Vehicule.hpp"
#include "Battery.h"
#include "Color.hpp"
#include "main.hpp"
class VahiculeElectrique: public virtual Vehicule {
public:
    VahiculeElectrique(string intitule_, Color color_, Point3D point3d_,float speed_ = 0, float e=50):Vehicule(intitule_, e,color_,point3d_), speed(speed_){};
    VahiculeElectrique(const VahiculeElectrique& orig):speed(orig.speed), pile(orig.pile){};
    virtual float getSpeed(){return this->speed;};
    void setSpeed(float speed) {this->speed = speed;}

    virtual float getEnergyUsage(float distance_){return (distance_ * 1.58); };
    virtual ~VahiculeElectrique(){};
private:
    Battery pile;
    float speed;
};

#endif	/* VAHICULEELECTRIQUE_HPP */

