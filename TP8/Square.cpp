/* 
 * File:   Square.cpp
 * Author: francois
 * 
 * Created on November 18, 2013, 3:46 PM
 */

#include "Square.hpp"


Square::~Square() {
        cout << "~Square() :" << this << endl;
}
Square::Square(float c_, Point3D& center_, Color& color_){
    cout << "Square(c, center, color) :" << this << endl;
    this->setC(c_);
    this->setCenter(center_);
    this->setColor(color_);
}
Square::Square(float x_, float y_, float z_) {
    this->center.setX(x_);
    this->center.setY(y_);
    this->center.setZ(z_);
}
float Square::getArea() const {
    return (this->getC()*this->getC());
}
void Square::draw(MyWindow* win) const {
    float centerX = this->getCenter().getX();
    float centerY = this->getCenter().getY();
    float cote = this->getC();
    for (float x = centerX - cote/2; x <= centerX + cote/2; x++) {
        for (float y = centerY- cote/2; y <= centerY + cote/2; y++) {
            win->putPixel(x, y, this->getColor().GetR(), this->getColor().GetG(), this->getColor().GetB());
        }
    }
    
}
