/* 
 * File:   VehiculeEssence.hpp
 * Author: francois
 *
 * Created on December 2, 2013, 2:47 PM
 */

#ifndef VEHICULEESSENCE_HPP
#define	VEHICULEESSENCE_HPP
#include "Turbo.h"
#include "Vehicule.hpp"
#include "main.hpp"
class VehiculeEssence : public virtual Vehicule {
public:
    VehiculeEssence(string name_="default", Color color_=Color(255, 255, 255), Point3D point_=Point3D(0,0,0),float speed_=0, float e=150): speed(speed_), Vehicule(name_, e, color_, point_){};
    VehiculeEssence(const VehiculeEssence& orig){};

    virtual float getEnergyUsage(float distance_) { return distance_*2,72;};
    virtual float getSpeed() {return this->speed;};
    void setSpeed(float speed) {
        this->speed = speed;
    }
    virtual ~VehiculeEssence(){};
private:
    Turbo motor;
    float speed;
};

#endif	/* VEHICULEESSENCE_HPP */

