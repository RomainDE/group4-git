/* 
 * File:   BadColor.h
 * Author: francois
 *
 * Created on November 25, 2013, 3:18 PM
 */

#ifndef BADCOLOR_H
#define	BADCOLOR_H

#include "main.hpp"
class BadColor{
private:
    int color;
    string ColorName;
public:
    BadColor(int orig = 0, string msg = ""):color(orig), ColorName(msg) {};
    BadColor(const BadColor &orig) {this->color = orig.getColor(); this->ColorName = orig.getColorName();};
    string getColorName() const {
        return ColorName;
    }

    int getColor() const {
        return color;
    }

    void print(){cout << "Color = " << this->getColorName() << " : " << this->getColor() << endl;};
    ~BadColor(){};
};

#endif	/* BADCOLOR_H */

