/* 
 * File:   Shape.cpp
 * Author: francois
 * 
 * Created on November 4, 2013, 1:49 PM
 */

#include "Shape.hpp"
#include <iostream>
using namespace std;
int Shape::idShape = 0;
Shape::Shape() {
    this->id = getId();
    this->setColor(0,0,0);
    cout << "Shape() : " << this->id <<endl;
}

Shape::Shape(const Color& orig) {
    this->id = getId();
    this->setColor(orig);
    cout << "Shape(const Color&) : " << this->id << endl;
}

Shape::Shape(const Shape& orig) {
    
    this->id = getId();
    this->setColor(orig.getColor());
    cout << "Shape(const Shape&) : " << this->id << endl;
}

Shape::~Shape() {
    cout << "~Shape() : " << this->id << endl;
}
int Shape::getId() {
    idShape++;
    return idShape;
}
float Shape::getArea() const {
}