/* 
 * File:   Disc.cpp
 * Author: edouard
 * 
 * Created on 4 novembre 2013, 13:52
 */

#include "Disc.hpp"
#include <iostream>
#include "BadRadius.h"
using namespace std;
    Disc::Disc():center(), radius(){
        
    }
    Disc::Disc(float x_, float y_, float z_) : radius(), center(x_, y_, z_) {
        
    }
    Disc::Disc(float radius,const Point3D& center_, const Color color_):radius(radius), center(center_){
        if (radius < 0) {
            throw BadRadius(radius);
        }
        this->setColor(color_.GetR(), color_.GetG(), color_.GetB());
        cout << "Disc" << endl;
    }
    
    Disc::Disc(const Disc&){
        
    }
    
    Disc::~Disc(){
        this->center.~Point3D();
        cout <<  "~Disc" << endl;
    }

    const Point3D& Disc::getCenter() const {
        return center;
 }

    void Disc::setCenter(const Point3D& center_) {
        this->center.setX(center_.getX());
        this->center.setY(center_.getY());
        this->center.setZ(center_.getZ());
    }

    const float& Disc::getRadius() const {
        return radius;
    }

    void Disc::setRadius(const float& radius) {
        if (radius < 0) {
            throw BadRadius(radius);
        }
        this->radius = radius;
    }

    float Disc::getArea() const{
        return M_PI*(this->radius)*(this->radius);
    }
    
    void Disc::draw(MyWindow* win) const{
        cout << "Drawing Disc" << endl;
        Color c = this->getColor();
        float radius = this->getRadius();
        float centerX = this->getCenter().getX();
        float centerY = this->getCenter().getY();
        float borneXLeft = centerX - radius - 1;
        if(borneXLeft < 0){
            borneXLeft = 0;
        }
        float borneXRight = centerX + radius + 1;
        if(borneXRight > (float)win->getWidth() ){
            borneXRight = (float)win->getWidth()-1;
        }
        float borneYLeft = centerY - radius - 1;
        if(borneYLeft < 0){
            borneYLeft = 0;
        }
        float borneYRight = centerY + radius + 1;
        if(borneYRight > (float)win->getHeight()){
            borneYRight = (float)win->getHeight()-1;
        }
        
        for (int x = borneXLeft; x <= borneXRight; x++) {
            for (int y = borneYLeft; y <= borneYRight; y++) {
                if (((x-centerX)*(x-centerX) + (y-centerY)*(y-centerY)) <= (radius*radius) ){
                    win->putPixel(x, y, c.GetR(), c.GetG(), c.GetB());
                }
            }
        }
        
    }
