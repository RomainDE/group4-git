/* 
 * File:   Paddock.hpp
 * Author: francois
 *
 * Created on December 16, 2013, 1:54 PM
 */

#ifndef PADDOCK_HPP
#define	PADDOCK_HPP
#include "main.hpp"
template <class T> class Paddock {
public:
    Paddock(int size_=0) { 
        string s = typeid(T).name();
        cout << "Circuit pour " << s << endl;
    };
    Paddock(const Paddock& orig){};

    T* GetCircuit() const {
        return circuit;
    }

    virtual ~Paddock(){
    };
private:
    list<T> circuit;
};

#endif	/* PADDOCK_HPP */

