/* 
 * File:   ShapeException.h
 * Author: francois
 *
 * Created on November 25, 2013, 2:15 PM
 */

#ifndef SHAPEEXCEPTION_H
#define	SHAPEEXCEPTION_H
#include "main.hpp"
#include "Shape.hpp"
class ShapeException {
private:
    int id;
public:
    ShapeException (int id_=0):id(id_){};
    ShapeException(const ShapeException & orig ):id(orig.id){};
    
     virtual void print(){cout<<"shape exception print"<<endl;};
    
    ~ShapeException(){};
};


#endif	/* SHAPEEXCEPTION_H */

