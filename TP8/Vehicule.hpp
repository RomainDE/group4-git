#ifndef VEHICULE_HPP
#define VEHICULE_HPP
#include "main.hpp"
#include "MyDrawable.hpp"
#include "Point3D.hpp"
#include "Shape.hpp"
class Vehicule: public MyDrawable{
private:
    Point3D position;
    string name;
    Shape* representation;
protected:
    float energy;
    Vehicule();
public:
    Vehicule(const string&,float,const Color&,const Point3D&);
    Vehicule(const Vehicule&);
    
    void draw(MyWindow*) const;
    void setRepresentation(Shape *);
    void moveTurn();
    
    virtual float getSpeed() = 0;
    virtual float getEnergyUsage(float) = 0;
    friend ostream & operator<<(ostream&, const Vehicule&);
    void print() const;
    
    const float & getEnergy() const;
    const Point3D & getPosition() const;
    
    void setEnergy(float energy_) {
        this->energy = energy_;
    }


    ~Vehicule();
};

#endif
