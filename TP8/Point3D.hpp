/* 
 * File:   Pointed3D.hpp
 * Author: elerion
 *
 * Created on 4 novembre 2013, 14:16
 */

#ifndef POINTED3D_HPP
#define	POINTED3D_HPP
#include "Color.hpp"

class Point3D {
private:
            float x,y,z;
            static int nbInst;
	public:
            Point3D(const float=0,const float=0,const float=0);
            Point3D(const Point3D &);
            ~Point3D();
            void initialize(const float=0,const float=0,const float=0);
            void reset(); //reset coord to 0,0,0
            float moveTo(Point3D&); //move to another point, returns move distance
            float moveTo(float=0,float=0,float=0); //move to coords, returns distance
            void decr(); //decrease all coords with -1
            void incr(); //increase all coords with +1
            void print()const; // display a Point in the following way: (x,y,z) - EnumColor
            const float getX() const;
            const float getY() const;
            const float getZ() const;
            void setX(const float);
            void setY(const float);
            void setZ(const float);
            void setPoint(const Point3D& orig){
                this->setX(orig.getX());
                this->setY(orig.getY());
                this->setZ(orig.getZ());
            };
            void createRoad(int);
            Point3D & getPoint3DOnRoad(int, int);
            float distanceRoad(int);
            
            Point3D operator=(const Point3D& orig){
                this->setPoint(orig);
                return *this;
            }
            
            static int getNbInst();

};

#endif	/* POINTED3D_HPP */

