/* 
 * File:   ShapeContainer.hpp
 * Author: francois
 *
 * Created on November 25, 2013, 3:57 PM
 */

#ifndef SHAPECONTAINER_HPP
#define	SHAPECONTAINER_HPP
#include "main.hpp"
#include "Shape.hpp"
class ShapeContainer : public Shape {
public:
    ShapeContainer(Shape* orig = NULL){
        if(orig != NULL){
            shapeArray = new Shape*[1];
            shapeArray[0] = orig;
        }else{
            shapeArray = new Shape*[1];
        }
    };
    ShapeContainer(const ShapeContainer& orig) {
        this->shapeArray = new Shape[sizeof(orig.shapeArray)];
        for (int i = 0; i < sizeof(orig.shapeArray); i++) {
            this->shapeArray[i] = orig.shapeArray[i];
        }
    }
    Shape* GetShapeArray(int i) const {
        if (i < sizeof(this->shapeArray) || i > 0) {
              return shapeArray[i];
        }else { return NULL;}
    }

    void SetShapeArray(Shape shape_, int i) {
          if (i < sizeof(this->shapeArray) || i > 0) {
              this->shapeArray[i] = shape_;
        }
    }
    void pushShape (Shape shape) {
        int i = 0;
        Shape * tmpTab = new Shape[sizeof(this->shapeArray)+1];
        for (i = 0; i < sizeof(this->shapeArray); i++ ) {
            tmpTab[i] = this->GetShapeArray(i);
        }
        tmpTab[i] = shape;
        delete[] this->shapeArray;
        this->shapeArray = tmpTab;
        tmpTab = NULL;  
    }
    Shape removeShape (const int i_) {
        Shape* tmpShape = new Shape(sizeof(this->shapeArray) - 1);
        if(i_ > 0 && i_ < sizeof(this->shapeArray)){
            if(i_ == 0){
               for(int i =1; i<sizeof(this->shapeArray); i++){
                   tmpShape[i-1] = this->GetShapeArray(i);
               } 
            }else if(i_ == (sizeof(this->shapeArray)-1)){
                for(int i=0; i<sizeof(this->shapeArray)-1; i++){
                    tmpShape[i] = this->GetShapeArray(i);
                }
            }else{
                for(int i=0; i<i_; i++){
                    tmpShape[i] = this->GetShapeArray(i);
                }
                for(int i=i_+1; i<sizeof(this->shapeArray);i++){
                    tmpShape[i] = this->GetShapeArray(i);
                }
            }
            delete[] this->shapeArray;
            this->shapeArray = tmpShape;
            tmpShape = NULL;
        }
    }

    virtual ~ShapeContainer() { delete[] this->shapeArray; };
private:
    Shape** shapeArray;

};

#endif	/* SHAPECONTAINER_HPP */

