/* 
 * File:   Ellipse.hpp
 * Author: edouard
 *
 * Created on 18 novembre 2013, 15:51
 */

#ifndef ELLIPSE_HPP
#define	ELLIPSE_HPP
#include"Point3D.hpp"
#include "Shape.hpp"
#include "BadEllipse.h"
class Ellipse:public Shape {
    private:
    Point3D center;
    float radius1;
    float radius2;
    
public:
    Ellipse();
    Ellipse(float x_=0, float y_=0, float z_=0);
    Ellipse(float radius1, float radius2, const Point3D& center, const Color);
    Ellipse(const Ellipse&);
    const float & getRadius1()const;
    const float & getRadius2()const;
    void setRadius1(const float &);
    void setRadius2(const float &);
    const Point3D & getCenter()const;
    void setCenter(const Point3D &);
    void move (float x_, float y_) {this->setCenter(Point3D(x_, y_));};
    float getArea() const;
    void draw(MyWindow*) const;
    
    Ellipse operator=(const Ellipse& orig){
        this->center = orig.getCenter();
        this->radius1 = orig.radius1;
        this->radius2 = orig.radius2;
        return *this;
    }
    
    virtual ~Ellipse();


};

#endif	/* ELLIPSE_HPP */

