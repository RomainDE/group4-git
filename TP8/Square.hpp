/* 
 * File:   Square.hpp
 * Author: francois
 *
 * Created on November 18, 2013, 3:46 PM
 */

#ifndef SQUARE_HPP
#define	SQUARE_HPP
#include "Point3D.hpp"
#include "MyWindow.hpp"
#include "Shape.hpp"
class Square : public Shape {
public:
    Square(float c_=0):c(c_){};
    Square(const Square& orig):c(orig.c),center(orig.center){};
    Square(float c_, Point3D& center_, Color& color_);
    Square(float x_ = 0, float y_=0, float z_=0);
    float getC() const {
        return c;
    }

    void setC(float c) {
        this->c = c;
    }

    Point3D getCenter() const {
        return center;
    }

    void setCenter(Point3D center) {
        this->center = center;
    }
    float getArea() const;
    void draw(MyWindow*) const;
    void move(float x_, float y_){
        this->setCenter(Point3D(x_, y_));
    }
    virtual ~Square();
    
    Square operator=(const Square& orig){
        this->setC(orig.getC());
    }
private:
    float c;
    Point3D center;
};

#endif	/* SQUARE_HPP */

