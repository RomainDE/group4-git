/* 
 * File:   BadWindow.hpp
 * Author: elerion
 *
 * Created on 25 novembre 2013, 14:13
 */

#ifndef BADWINDOW_HPP
#define	BADWINDOW_HPP
#include "main.hpp"
class BadWindow {
public:
    BadWindow(int x_ =0, int y_ =0, int width_ =0, int height_ =0, string msg_ =""):x(x_), y(y_), width(width_), height(height_), msg(msg_){cout << "constructor BadWidow" << endl;};
    BadWindow(const BadWindow& orig):x(orig.GetX()), y(orig.GetY()), width(orig.GetWidth()), height(orig.GetHeight()), msg(orig.GetMsg()){cout << "Copy constructeur BadWindow" << endl;};
    virtual ~BadWindow(){cout << "~BadWindow" << endl;};

    int GetHeight() const {return height;}
    string GetMsg() const {return msg;}
    int GetWidth() const {return width;}
    int GetX() const {return x;}
    int GetY() const {return y;}
    
    void print(){
        cout << "Exception of Window : " << this << endl;
        cout << "x : " << this->GetX() << endl << "y : " << this->GetY() << endl << "width : " << this->width << endl << "height : " << this->GetHeight() << endl << "msg : " << this->GetMsg() << endl;
    }

private:
    int x;
    int y;
    int width;
    int height;
    string msg;
};

#endif	/* BADWINDOW_HPP */

