/* 
 * File:   MyDrawable.cpp
 * Author: francois
 * 
 * Created on November 18, 2013, 1:44 PM
 */

#include "MyDrawable.hpp"

MyDrawable::MyDrawable() : c() {
    cout << "MyDrawable : " << this << endl;
}

MyDrawable::MyDrawable(const MyDrawable& orig) {
    this->c.SetColor(orig.getColor());
    cout <<"MyDrawable(MyDrawble&) :" << this << endl;
}
MyDrawable::MyDrawable(const Color& orig) {
    this->c.SetColor(orig);
    cout <<"MyDrawable(Color&)"<< this << endl;
}

MyDrawable::~MyDrawable() {
}

