/* 
 * File:   Rectangle.cpp
 * Author: elerion
 * 
 * Created on 18 novembre 2013, 15:47
 */

#include "Rectangle.hpp"

void Rectangle::draw(MyWindow* win) const{
    cout << "Drawing Rectangle" << endl;
    Color c = this->getColor();
    float centerX = this->GetCenter().getX();
    float centerY = this->GetCenter().getY();
    float borneXLeft = centerX - (this->GetL()/2);
    float borneXRight = centerX + (this->GetL()/2);
    float borneYLeft = centerY - (this->Getl()/2);
    float borneYRight = centerY + (this->Getl()/2);
    if (borneXLeft < 0) {
        borneXLeft = 0;
    }
    if (borneXRight > (float) win->getWidth()) {
        borneXRight = (float) win->getWidth()-1;
    }
    if (borneYLeft < 0) {
        borneYLeft = 0;
    }
    if (borneYRight > (float) win->getHeight()) {
        borneYRight = (float) win->getHeight()-1;
    }
    for(float x = borneXLeft; x <= borneXRight; x++){
        for(float y = borneYLeft; y <= borneYRight; y++){
            win->putPixel(x, y, c.GetR(), c.GetG(), c.GetB());
        }
    }
}

