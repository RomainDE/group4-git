/* 
 * File:   Pointed3D.cpp
 * Author: elerion
 * 
 * Created on 4 novembre 2013, 14:16
 */

#include "Point3D.hpp"

int Point3D::nbInst = 0;
int Point3D::getNbInst() {
    return nbInst;
}

Point3D::Point3D(const Point3D& orig) {
    initialize(orig.getX(), orig.getY(), orig.getZ());
    nbInst++;
}
Point3D::Point3D(const float x, const float y, const float z){
    initialize(x, y, z);
    nbInst++;
}
Point3D::~Point3D(){
    nbInst--;
}
void Point3D::initialize(const float x,const float y, const float z){
    this->setX(x);
    this->setY(y);
    this->setZ(z);
}
void Point3D::reset() {
    this->initialize();
} //reset coord to 0,0,0
float Point3D::moveTo(Point3D & point){
    return moveTo(point.getX(), point.getY(), point.getZ());
} //move to another point, returns move distance
float Point3D::moveTo(float x_,float y_,float z_) { //move to coords, returns distance
    float distance;
    distance = sqrt(pow((x_-this->getX()),2) + pow((y_-this->getY()), 2) + pow((z_-this->getZ()),2));
    this->setX(x_);
    this->setY(y_);
    this->setZ(z_);
    return distance;
}
void Point3D::decr() { //decrease all coords with -1
    this->setX(this->getX()-1);
    this->setY(this->getY()-1);
    this->setZ(this->getZ()-1);  
}
void Point3D::incr() { //increase all coords with +1
    this->setX(this->getX()+1);
    this->setY(this->getY()+1);
    this->setZ(this->getZ()+1); 
}
void Point3D::print()const{
    cout <<"(" << this->getX() << "," << this->getY() << "," << this->getZ() << ") \\ color : ";
} // display a Point in the following way: (x,y,z) - EnumColor
const float Point3D::getX() const{
    return this->x ;
}
const float Point3D::getY() const{
    return this->y;
}
const float Point3D::getZ() const{
    return this->z;
}
void Point3D::setX(const float x) {
    this->x = x;
}
void Point3D::setY(const float y) {
    this->y = y;
}
void Point3D::setZ(const float z) {
    this->z = z;
}
void Point3D::createRoad(int num_of_point){
//    Point3D* ptr_Point3D=new Point3D[num_of_point];
    for(int i=0; i < num_of_point; i++){
        this[i].initialize((float)1+rand()%9, (float)1+rand()%9, (float)1+rand()%9);
//        this[i].print();
    }
}

Point3D & Point3D::getPoint3DOnRoad(int index, int size){
    if(index < size){
        return this[index];
    }
    return this[0];
}

float Point3D::distanceRoad(int size){
    float dist = 0;
    return dist;
            
}
