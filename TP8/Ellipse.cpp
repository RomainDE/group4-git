/* 
 * File:   Ellipse.cpp
 * Author: edouard
 * 
 * Created on 18 novembre 2013, 15:51
 */
#include "main.hpp"
#include "Ellipse.hpp"
#include <iostream>
using namespace std;
Ellipse::Ellipse():center(), radius1(){
        
    }
    Ellipse::Ellipse(float x_, float y_, float z_) : radius1(), radius2(), center(x_, y_, z_) {
        
    }
    Ellipse::Ellipse(float radius1, float radius2, const Point3D& center_, const Color color):radius1(radius1), radius2(radius2), center(center_){
        if ((radius1<0) || (radius2 < 0)) {
            throw BadEllipse(radius1, radius2);
        }
        cout << "Ellipse" << endl;
    }
    
    Ellipse::Ellipse(const Ellipse&){
        
    }
    
    Ellipse::~Ellipse(){
        this->center.~Point3D();
        cout <<  "~Ellipse" << endl;
    }

    const Point3D& Ellipse::getCenter() const {
        return center;
 }

    void Ellipse::setCenter(const Point3D& center_) {
        this->center.setX(center_.getX());
        this->center.setY(center_.getY());
        this->center.setZ(center_.getZ());
    }

    const float& Ellipse::getRadius1() const {
        return radius1;
    }

    const float& Ellipse::getRadius2() const {
        return radius2;
    }
    
    void Ellipse::setRadius1(const float& radius1) {
        if (radius1<0) {
            throw BadEllipse(radius1, this->getRadius2());
        }
        this->radius1 = radius1;
    }
    
    void Ellipse::setRadius2(const float& radius2) {
        if (radius2 < 0) {
            throw BadEllipse(this->getRadius1(), radius2);
        }
        this->radius2 = radius2;
    }

    float Ellipse::getArea() const{
        return M_PI*(this->radius1)*(this->radius2);
    }
    
    void Ellipse::draw(MyWindow* win) const{
        cout << "Drawing Ellipse" << endl;
        Color c = this->getColor();
        float radius1 = this->getRadius1();
        float radius2 = this->getRadius2();
        float centerX = this->getCenter().getX();
        float centerY = this->getCenter().getY();
        float borneXLeft = centerX - max(radius1, radius2) - 1;
        float borneXRight = centerX + max(radius1, radius2) + 1;
        float borneYLeft = centerY - max(radius1, radius2) - 1;
        float borneYRight = centerY + max(radius1, radius2) + 1;
        if (borneXLeft < 0) {
            borneXLeft = 0;
        }
        if (borneXRight > (float) win->getWidth()) {
            borneXRight = (float) win->getWidth() -1;
        }
        if (borneYLeft < 0) {
            borneYLeft = 0;
        }
        if (borneYRight > (float) win->getHeight()) {
            borneYRight = (float) win->getHeight() -1;
        }
        
        for (int x = borneXLeft; x <= borneXRight; x++) {
            for (int y = borneYLeft; y <= borneYRight; y++) {
                if ((((x-centerX)*(x-centerX))/(radius1*radius1)) + ((y-centerY)*(y-centerY))/(radius2*radius2) <= 1 ){
                    win->putPixel(x, y, c.GetR(), c.GetG(), c.GetB());
                }
            }
        }
        
    }

