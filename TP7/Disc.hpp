/* 
 * File:   DIsc.hpp
 * Author: edouard
 *
 * Created on 4 novembre 2013, 13:51
 */

#ifndef DISC_HPP
#define	DISC_HPP

   class Disc {
private:
    Point3D center;
    float radius;

public:
    Disc();
    Disc(float radius,const Point3D& center);
    Disc(const Disc&);
    const float & getRadius();
    void setRadius(const float &);
    const Point3D & getCenter();
    void setCenter(const Point3D &);
    float getArea() const;
    void draw() const;
    ~Disc();
   }

#endif	/* DISC_HPP */

