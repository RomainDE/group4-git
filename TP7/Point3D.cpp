/* 
 * File:   Pointed3D.cpp
 * Author: elerion
 * 
 * Created on 4 novembre 2013, 14:16
 */

#include "Point3D.hpp"

int Point3D::nbInst = 0;
int Point3D::getNbInst() {
    return nbInst;
}

Point3D::Point3D(){
    initialize();
    nbInst++;
}
Point3D::Point3D(const float x, const float y, const float z, const int r, const int g, const int b){
    initialize(x, y, z, r, g, b);
    nbInst++;
}
Point3D::~Point3D(){
    nbInst--;
}
void Point3D::initialize(const int r, const int g, const int b,const float x,const float y):color(r,g,b){
    this->setX(x);
    this->setY(y);
    this->setZ(z);
    this->setColor(color);
}
void Point3D::reset() {
    this->initialize();
} //reset coord to 0,0,0
float Point3D::moveTo(Point3D & point){
    return moveTo(point.getX(), point.getY(), point.getZ());
} //move to another point, returns move distance
float Point3D::moveTo(float x,float y,float z) { //move to coords, returns distance
    float distance;
    distance = sqrt(pow((x-this->getX()),2) + pow((y-getY()), 2) + pow((z-getZ()),2));
    setX(x);
    setY(y);
    setZ(z);
    return distance;
}
void Point3D::decr() { //decrease all coords with -1
    this->setX(this->getX()-1);
    this->setY(this->getY()-1);
    this->setZ(this->getZ()-1);  
}
void Point3D::incr() { //increase all coords with +1
    this->setX(this->getX()+1);
    this->setY(this->getY()+1);
    this->setZ(this->getZ()+1); 
}
void Point3D::print(){
    float X, Y, Z;
    X = this->getX();
    Y = this->getY();
    Z = this->getZ();
    cout <<"(" << X << "," << Y << "," << Z << ") \\ color : " << this->getColor().getName() << endl;
} // display a Point in the following way: (x,y,z) - EnumColor
void Point3D::colorize(Color color){
    this->color = color;
}
const float Point3D::getX() {
    return this->x ;
}
const float Point3D::getY() {
    return this->y;
}
const float Point3D::getZ() {
    return this->z;
}
const Color Point3D::getColor() {
    return this->color;
}
void Point3D::setX(const float x) {
    this->x = x;
}
void Point3D::setY(const float y) {
    this->y = y;
}
void Point3D::setZ(const float z) {
    this->z = z;
}
void Point3D::setColor( const int r, const int g, const int b) {
    this->color.SetColor(r, g, b);
}
void Point3D::createRoad(int num_of_point){
//    Point3D* ptr_Point3D=new Point3D[num_of_point];
    for(int i=0; i < num_of_point; i++){
        this[i].initialize((float)1+rand()%9, (float)1+rand()%9, (float)1+rand()%9, (int)1+rand()%9, (int)1+rand()%9, (int)1+rand()%9);
//        this[i].print();
    }
}

Point3D & Point3D::getPoint3DOnRoad(int index, int size){
    if(index < size){
        return this[index];
    }
    return this[0];
}

float Point3D::distanceRoad(int size){
    float dist = 0;
    return dist;
            
}
