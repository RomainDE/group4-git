/* 
 * File:   Disc.cpp
 * Author: edouard
 * 
 * Created on 4 novembre 2013, 13:52
 */

#include "Disc.hpp"

    Disc::Disc(){
        
    }
    
    Disc::Disc(float radius,const Point3D& center){
        this->radius = radius;
        this->center = center;
        cout << "Disc" << endl;
    }
    
    Disc::Disc(const Disc&){
        
    }
    
    Disc::~Disc(){
        ~Point3D();
        cout <<  "~Disc" << endl;
    }

    Point3D Disc::getCenter() const {
        return center;
 }

    void Disc::setCenter(Point3D center) {
        this->center = center;
    }

    float Disc::getRadius() const {
        return radius;
    }

    void Disc::setRadius(float radius) {
        this->radius = radius;
    }

    float Disc::getArea(){
        return pi*(this->radius)*(this->radius);
    }
    
    void Disc::draw() const{
        this->center.print();
        cout << this->radius << endl;
    }
