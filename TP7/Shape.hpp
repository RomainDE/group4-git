/* 
 * File:   Shape.hpp
 * Author: francois
 *
 * Created on November 4, 2013, 1:49 PM
 */

#ifndef SHAPE_HPP
#define	SHAPE_HPP
#include "Color.hpp"

class Shape {
private:
    int id;
    static int idShape;
protected:
    Color c;
    int getNewId();

public:
    Shape();
    Shape(const Color &);
    Shape(const Shape &);
    void setColor(int r, int g, int b);
    void setColor(const Color&);
    const Color & getColor() const;
    float getArea() const;
    void draw() const;
    // you can add your own methods here
    ~Shape();
};


#endif	/* SHAPE_HPP */

