/* 
 * File:   Color.hpp
 * Author: elerion
 *
 * Created on 4 novembre 2013, 13:52
 */

#ifndef COLOR_HPP
#define	COLOR_HPP
#include "main.hpp"
class Color {
public:
    Color(int R = 0, int G = 0, int B = 0):r(0), g(0), b(0){this->SetColor(R, G, B);}  
    Color(const Color& orig):r(0), g(0), b(0){this->SetColor(orig.GetR(), orig.GetG(), orig.GetB());}
    virtual ~Color(){};
    const int GetB() const {return b;}
    const int GetG() const {return g;}
    const int GetR() const {return r;}
    void SetColor(const int r, const int g, const int b){ this->SetR(r); this->SetG(g); this->SetB(b);}
    void print(){std::cout << "R = " << this->GetR() << " G = " << this->GetG() << " B  = " << this->GetB() << endl;}
    
private:
    int r;
    int g;
    int b;
    void SetB(const int b) {
        if ((b >= 0) && (b<= 255)){
            this->b = b;
        }else{
            this->b = 0;
        }
    }
    void SetG(const int g) {
        if ((b >= 0) && (b<= 255)){
            this->g = g;
        }else{
            this->g = 0;
        }
    }
    void SetR(const int r) {
        if ((b >= 0) && (b<= 255)){
            this->r = r;
        }else{
            this->r = 0;
        } 
    }
    
    
};

#endif	/* COLOR_HPP */

