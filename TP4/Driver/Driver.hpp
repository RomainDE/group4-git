/* 
 * File:   Driver.hpp
 * Author: elerion
 *
 * Created on 23 septembre 2013, 14:36
 */

#ifndef DRIVER_HPP
#define	DRIVER_HPP
#include <iostream>
#include <string.h>
using namespace std;

class Driver{
private:
    char *name;
    int number;
    static int nbInst;
    
public:
    Driver();
    Driver(char*, int);
    ~Driver();
    void init(const char * =" ", const int =0) ;
    void init(Driver &);
    const char * getName() ;
    const int getNumber() ;
    void setName(const char*) ;
    void setNumber(const int) ;
    bool testEquality(Driver&,Driver &) ;
    bool testEquality(const char*) ;
    bool testEquality(const int) ;
    void copy(Driver* DriverPtr, Driver aDriver) ;
    void print() ;
    static int getNbInst();
};


#endif	/* DRIVER_HPP */

