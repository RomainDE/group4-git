
#include "Driver.hpp"
int Driver::nbInst=0;
int Driver::getNbInst(){
    return nbInst;
}

Driver::Driver(){
    init();
    nbInst++;
}

Driver::Driver(char *name, int number){
    init(name, number);
    nbInst++;
}

Driver::~Driver(){
    nbInst--;
}

void Driver::init(const char *name, const int number){
    this->setName(name);
    this->setNumber(number);
}

const char * Driver::getName(){
	return this->name;
}

const int Driver::getNumber(){
	return this->number;
}

void Driver::setName(const char * name){
	this->name = new char[sizeof(name)];
        strcpy(this->name, name);
}

void Driver::setNumber(const int number){
    this->number = number;
}

bool Driver::testEquality(Driver &a, Driver &b){
    if(a.testEquality(b.getName()) && a.testEquality(b.getNumber())){
        return true;
    }
    return false;
}

bool Driver::testEquality(const char *name){
	if (strcmp(this->getName(), name) == 0){
		return true;
	}
	return false;
}

bool Driver::testEquality(int number){
	if(this->getNumber() == number){
		return true;
	}
	return false;
}

void Driver::copy(Driver *driverPtr, Driver aDriver){
	if (testEquality(aDriver, *(driverPtr)) != 1){
		driverPtr->init(aDriver.getName(), aDriver.getNumber());
	}
}

void Driver::print(){
	cout<< "Driver : " << this->getName() << "\\ " << this->getNumber() <<endl;
}
